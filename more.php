<?php include 'header.php'; ?>

        <div class="mh-header-nav-mobile clearfix"></div>
          <div class="mh-wrapper clearfix">
            <div class="mh-main clearfix">
                <div id="main-content" class="mh-loop mh-content" role="main">
                    <nav class="mh-breadcrumb">
                      <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="index.php" itemprop="url">
                          <span itemprop="title">Home</span>
                        </a>
                        </span>
                        <span class="mh-breadcrumb-delimiter">
                          <i class="fa fa-angle-right"></i>
                        </span>
                        Politics
                      </nav>
                    <header class="page-header">
                        <h1 class="page-title">Politics</h1>
                    </header>
                    <article class="mh-posts-list-item clearfix post-169 post type-post status-publish format-standard has-post-thumbnail hentry category-politics tag-conflicts tag-middle-east tag-military tag-politics tag-war">
                        <figure class="mh-posts-list-thumb">
                            <a class="mh-thumb-icon mh-thumb-icon-small-mobile" href="single.php">
                              <img width="326" height="245" src="../../wp-content/uploads/sites/16/2015/10/army-326x245.jpg" class="attachment-mh-magazine-medium size-mh-magazine-medium wp-post-image" alt="Army" srcset="http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/army-326x245.jpg 326w, http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/army-80x60.jpg 80w" sizes="(max-width: 326px) 100vw, 326px" /> </a>
                            <div class="mh-image-caption mh-posts-list-caption"> Politics</div>
                        </figure>
                        <div class="mh-posts-list-content clearfix">
                            <header class="mh-posts-list-header">
                                <h3 class="entry-title mh-posts-list-title"> <a href="single.php"> Kalil delenit augue duis dolore hetura </a></h3>
                                <div class="mh-meta entry-meta"> <span class="entry-meta-date updated"><i class="fa fa-clock-o"></i><a href="single.php">Oct 16, 2015</a></span>
                                  <span class="entry-meta-author author vcard"><i class="fa fa-user"></i><a class="fn" href="single.php">John Doe</a></span>
                                  <span class="entry-meta-comments"><i class="fa fa-comment-o"></i><a href="single.php" class="mh-comment-count-link" >0</a></span></div>
                            </header>
                            <div class="mh-posts-list-excerpt clearfix">
                                <div class="mh-excerpt">
                                    <p>Tatvero accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea consetetur sadipscing elitr,
                                       sed diam nonumy eirmod takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure
                                        <a class="mh-excerpt-more" href="single.php">[&#8230;]</a></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="mh-posts-list-item clearfix post-170 post type-post status-publish format-standard has-post-thumbnail hentry category-politics tag-debate tag-government tag-law tag-legislation tag-politics">
                        <figure class="mh-posts-list-thumb">
                            <a class="mh-thumb-icon mh-thumb-icon-small-mobile" href="single.php">
                              <img width="326" height="245" src="../../wp-content/uploads/sites/16/2015/10/berlin-326x245.jpg" class="attachment-mh-magazine-medium size-mh-magazine-medium wp-post-image" alt="Berlin" srcset="http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/berlin-326x245.jpg 326w, http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/berlin-300x225.jpg 300w, http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/berlin-1024x768.jpg 1024w, http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/berlin-80x60.jpg 80w, http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/berlin.jpg 1280w" sizes="(max-width: 326px) 100vw, 326px" /> </a>
                            <div class="mh-image-caption mh-posts-list-caption"> Politics</div>
                        </figure>
                        <div class="mh-posts-list-content clearfix">
                            <header class="mh-posts-list-header">
                                <h3 class="entry-title mh-posts-list-title"> <a href="single.php"> Laoreet dolore magna aliquam erat vol magna aliquyam </a></h3>
                                <div class="mh-meta entry-meta"> <span class="entry-meta-date updated"><i class="fa fa-clock-o"></i><a href="single.php">Oct 11, 2015</a></span>
                                  <span class="entry-meta-author author vcard"><i class="fa fa-user"></i><a class="fn" href="single.php">Aline</a></span> <span class="entry-meta-comments"><i class="fa fa-comment-o"></i>
                                    <a href="single.php" class="mh-comment-count-link" >0</a></span></div>
                            </header>
                            <div class="mh-posts-list-excerpt clearfix">
                                <div class="mh-excerpt">
                                    <p>Tatvero accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea consetetur sadipscing elitr,
                                       sed diam nonumy eirmod takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure <a class="mh-excerpt-more" href="single.php">[&#8230;]</a></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="mh-posts-list-item clearfix post-178 post type-post status-publish format-video has-post-thumbnail hentry category-politics tag-government tag-politics tag-usa tag-white-house post_format-post-format-video">
                        <figure class="mh-posts-list-thumb">
                            <a class="mh-thumb-icon mh-thumb-icon-small-mobile" href="single.php"><img width="326" height="245" src="../../wp-content/uploads/sites/16/2015/10/united-states-326x245.jpg" class="attachment-mh-magazine-medium size-mh-magazine-medium wp-post-image" alt="United States" srcset="http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/united-states-326x245.jpg 326w, http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/united-states-300x225.jpg 300w, http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/united-states-1024x768.jpg 1024w, http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/united-states-80x60.jpg 80w, http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/united-states.jpg 1280w" sizes="(max-width: 326px) 100vw, 326px" /> </a>
                            <div class="mh-image-caption mh-posts-list-caption"> Politics</div>
                        </figure>
                        <div class="mh-posts-list-content clearfix">
                            <header class="mh-posts-list-header">
                                <h3 class="entry-title mh-posts-list-title"> <a href="single.php"> Consequat vel illum dolore eullamcorper suscipit lobort </a></h3>
                                <div class="mh-meta entry-meta"> <span class="entry-meta-date updated"><i class="fa fa-clock-o"></i><a href="single.php">Oct 10, 2015</a></span> <span class="entry-meta-author author vcard"><i class="fa fa-user"></i><a class="fn" href="single.php">Aline</a></span>
                                  <span class="entry-meta-comments"><i class="fa fa-comment-o"></i><a href="single.php" class="mh-comment-count-link" >0</a></span></div>
                            </header>
                            <div class="mh-posts-list-excerpt clearfix">
                                <div class="mh-excerpt">
                                    <p>Tatvero accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea consetetur sadipscing elitr,
                                       sed diam nonumy eirmod takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure
                                        <a class="mh-excerpt-more" href="single.php" title="Consequat vel illum dolore eullamcorper suscipit lobort">[&#8230;]</a></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    <div class="mh-loop-ad">
                        <div class="mh-ad-label">Advertisement</div>
                        <div class="mh-ad-area">
                            <div style="font-size: 13px; padding: 0.5em; background: #f5f5f5; border: 1px solid #ebebeb; text-align: center;">
                              <a target="_blank" href="single.php">Here you can place more advertisements and banners</a></div>
                        </div>
                    </div>
                    <article class="mh-posts-list-item clearfix post-177 post type-post status-publish format-standard has-post-thumbnail hentry category-politics tag-government tag-nsa tag-politics tag-surveillance">
                        <figure class="mh-posts-list-thumb">
                            <a class="mh-thumb-icon mh-thumb-icon-small-mobile" href="single.php">
                              <img width="326" height="245" src="../../wp-content/uploads/sites/16/2015/10/surveillance_camera-326x245.jpg" class="attachment-mh-magazine-medium size-mh-magazine-medium wp-post-image" alt="Surveillance Camera" srcset="http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/surveillance_camera-326x245.jpg 326w, http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/surveillance_camera-80x60.jpg 80w" sizes="(max-width: 326px) 100vw, 326px" /> </a>
                            <div class="mh-image-caption mh-posts-list-caption"> Politics</div>
                        </figure>
                        <div class="mh-posts-list-content clearfix">
                            <header class="mh-posts-list-header">
                                <h3 class="entry-title mh-posts-list-title"> <a href="single.php"> Atum zzril delenit augue duis dolore te feut augue </a></h3>
                                <div class="mh-meta entry-meta"> <span class="entry-meta-date updated"><i class="fa fa-clock-o"></i><a href="single.php">Oct 10, 2015</a></span> <span class="entry-meta-author author vcard"><i class="fa fa-user"></i><a class="fn" href="single.php">John Doe</a></span>
                                  <span class="entry-meta-comments"><i class="fa fa-comment-o"></i><a href="single.php" class="mh-comment-count-link" >0</a></span></div>
                            </header>
                            <div class="mh-posts-list-excerpt clearfix">
                                <div class="mh-excerpt">
                                    <p>Tatvero accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea consetetur sadipscing elitr, sed diam nonumy eirmod takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure <a class="mh-excerpt-more" href="../../atum-zzril-delenit-augue-duis-dolore-te-feut-augue/index.html" title="Atum zzril delenit augue duis dolore te feut augue">[&#8230;]</a></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="mh-posts-list-item clearfix post-176 post type-post status-publish format-standard has-post-thumbnail hentry category-politics tag-demonstration tag-government tag-legislation tag-politics">
                        <figure class="mh-posts-list-thumb">
                            <a class="mh-thumb-icon mh-thumb-icon-small-mobile" href="single.php"><img width="326" height="245" src="../../wp-content/uploads/sites/16/2015/10/demo-326x245.jpg" class="attachment-mh-magazine-medium size-mh-magazine-medium wp-post-image" alt="Demo" srcset="http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/demo-326x245.jpg 326w, http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/demo-80x60.jpg 80w" sizes="(max-width: 326px) 100vw, 326px" /> </a>
                            <div class="mh-image-caption mh-posts-list-caption"> Politics</div>
                        </figure>
                        <div class="mh-posts-list-content clearfix">
                            <header class="mh-posts-list-header">
                                <h3 class="entry-title mh-posts-list-title"> <a href="single.php" title="Duis dolore te feugait nulla facilisi feugaita serat" rel="bookmark"> Duis dolore te feugait nulla facilisi feugaita serat </a></h3>
                                <div class="mh-meta entry-meta"> <span class="entry-meta-date updated"><i class="fa fa-clock-o"></i>
                                  <a href="single.php">Oct 10, 2015</a></span>
                                  <span class="entry-meta-author author vcard"><i class="fa fa-user"></i>
                                    <a class="fn" href="single.php">Aline</a></span>
                                    <span class="entry-meta-comments"><i class="fa fa-comment-o"></i><a href="single.php" class="mh-comment-count-link" >0</a></span></div>
                            </header>
                            <div class="mh-posts-list-excerpt clearfix">
                                <div class="mh-excerpt">
                                    <p>Tatvero accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea consetetur sadipscing elitr,
                                       sed diam nonumy eirmod takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure
                                        <a class="mh-excerpt-more" href="single.php">[&#8230;]</a></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="mh-posts-list-item clearfix post-175 post type-post status-publish format-video has-post-thumbnail hentry category-politics tag-conflicts tag-crisis tag-middle-east tag-politics tag-war post_format-post-format-video">
                        <figure class="mh-posts-list-thumb">
                            <a class="mh-thumb-icon mh-thumb-icon-small-mobile" href="single.php"">
                              <img width="326" height="245" src="../../wp-content/uploads/sites/16/2015/10/military-326x245.jpg" class="attachment-mh-magazine-medium size-mh-magazine-medium wp-post-image" alt="Military" srcset="http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/military-326x245.jpg 326w, http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/military-80x60.jpg 80w" sizes="(max-width: 326px) 100vw, 326px" /> </a>
                            <div class="mh-image-caption mh-posts-list-caption"> Politics</div>
                        </figure>
                        <div class="mh-posts-list-content clearfix">
                            <header class="mh-posts-list-header">
                                <h3 class="entry-title mh-posts-list-title"> <a href="single.php"> Lobortis nisl ut aliquip ex ea commodo consequat </a></h3>
                                <div class="mh-meta entry-meta"> <span class="entry-meta-date updated"><i class="fa fa-clock-o"></i><a href="single.php">Oct 8, 2015</a></span> <span class="entry-meta-author author vcard"><i class="fa fa-user"></i><a class="fn" href="single.php">John Doe</a></span>
                                  <span class="entry-meta-comments">
                                  <i class="fa fa-comment-o"></i><a href="../../lobortis-nisl-ut-aliquip-ex-ea-commodo-consequat/index.html#respond" class="mh-comment-count-link" >0</a></span></div>
                            </header>
                            <div class="mh-posts-list-excerpt clearfix">
                                <div class="mh-excerpt">
                                    <p>Tatvero accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea consetetur sadipscing elitr,
                                       sed diam nonumy eirmod takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure
                                       <a class="mh-excerpt-more" href="single.php" title="Lobortis nisl ut aliquip ex ea commodo consequat">[&#8230;]</a></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    <div class="mh-loop-ad">
                        <div class="mh-ad-label">Advertisement</div>
                        <div class="mh-ad-area">
                            <div style="font-size: 13px; padding: 0.5em; background: #f5f5f5; border: 1px solid #ebebeb; text-align: center;">
                              <a target="_blank" href="single.php" title="Purchase MH Magazine Premium">Here you can place more advertisements and banners</a>
                            </div>
                        </div>
                    </div>
                    <article class="mh-posts-list-item clearfix post-173 post type-post status-publish format-standard has-post-thumbnail hentry category-politics tag-crisis tag-government tag-politics tag-refugees">
                        <figure class="mh-posts-list-thumb">
                            <a class="mh-thumb-icon mh-thumb-icon-small-mobile" href="single.php">
                              <img width="326" height="245" src="../../wp-content/uploads/sites/16/2015/10/flag-326x245.jpg" class="attachment-mh-magazine-medium size-mh-magazine-medium wp-post-image" alt="Flag" srcset="http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/flag-326x245.jpg 326w, http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/flag-80x60.jpg 80w" sizes="(max-width: 326px) 100vw, 326px" />
                            </a>
                            <div class="mh-image-caption mh-posts-list-caption"> Politics</div>
                        </figure>
                        <div class="mh-posts-list-content clearfix">
                            <header class="mh-posts-list-header">
                                <h3 class="entry-title mh-posts-list-title"> <a href="../../velit-esse-dolore-molestie-consequat-vel-illum-dolore/index.html" title="Velit esse dolore molestie consequat vel illum dolore" rel="bookmark"> Velit esse dolore molestie consequat vel illum dolore </a></h3>
                                <div class="mh-meta entry-meta">
                                  <span class="entry-meta-date updated">
                                    <i class="fa fa-clock-o"></i>
                                    <a href="single.php">Oct 6, 2015</a></span>
                                  <span class="entry-meta-author author vcard">
                                    <i class="fa fa-user"></i>
                                  <a class="fn" href="single.php">MH Themes</a>
                                </span>
                                  <span class="entry-meta-comments">
                                    <i class="fa fa-comment-o"></i>
                                    <a href="single.php" class="mh-comment-count-link" >0</a></span>
                                </div>
                            </header>
                            <div class="mh-posts-list-excerpt clearfix">
                                <div class="mh-excerpt">
                                    <p>Tatvero accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea consetetur
                                      sadipscing elitr, sed diam nonumy eirmod takimata sanctus
                                      est Lorem ipsum dolor sit amet. Duis autem vel eum iriure
                                      <a class="mh-excerpt-more" href="single.php">[&#8230;]</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="mh-posts-list-item clearfix post-172 post type-post status-publish format-standard has-post-thumbnail hentry category-politics tag-government tag-law tag-legislation tag-politics">
                        <figure class="mh-posts-list-thumb">
                            <a class="mh-thumb-icon mh-thumb-icon-small-mobile" href="single.php">
                              <img width="326" height="245" src="../../wp-content/uploads/sites/16/2015/10/federal_chancellery-326x245.jpg" class="attachment-mh-magazine-medium size-mh-magazine-medium wp-post-image" alt="Federal Chancellery" srcset="http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/federal_chancellery-326x245.jpg 326w, http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/federal_chancellery-80x60.jpg 80w" sizes="(max-width: 326px) 100vw, 326px" />
                            </a>
                            <div class="mh-image-caption mh-posts-list-caption"> Politics</div>
                        </figure>
                        <div class="mh-posts-list-content clearfix">
                            <header class="mh-posts-list-header">
                                <h3 class="entry-title mh-posts-list-title"> <a href="single.php"> Dolores duo eirmod eos erat et non eirmodoo </a></h3>
                                <div class="mh-meta entry-meta"> <span class="entry-meta-date updated"><i class="fa fa-clock-o"></i><a href="single.php">Oct 4, 2015</a></span>
                                  <span class="entry-meta-author author vcard"><i class="fa fa-user"></i><a class="fn" hhref="single.php">Aline</a></span>
                                  <span class="entry-meta-comments"><i class="fa fa-comment-o"></i><a hhref="single.php" class="mh-comment-count-link" >0</a></span></div>
                            </header>
                            <div class="mh-posts-list-excerpt clearfix">
                                <div class="mh-excerpt">
                                    <p>Tatvero accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea consetetur sadipscing elitr,
                                       sed diam nonumy eirmod takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure
                                        <a class="mh-excerpt-more" href="single.php">[&#8230;]</a></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="mh-posts-list-item clearfix post-171 post type-post status-publish format-standard has-post-thumbnail hentry category-politics tag-conference tag-government tag-politics">
                        <figure class="mh-posts-list-thumb">
                            <a class="mh-thumb-icon mh-thumb-icon-small-mobile" href="../../tempor-invidunt-ut-labore-et-dolore-magna/index.html"><img width="326" height="245" src="../../wp-content/uploads/sites/16/2015/10/bismarck-326x245.jpg" class="attachment-mh-magazine-medium size-mh-magazine-medium wp-post-image" alt="Bismarck" srcset="http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/bismarck-326x245.jpg 326w, http://demo.mhthemes.com/magazine/wp-content/uploads/sites/16/2015/10/bismarck-80x60.jpg 80w" sizes="(max-width: 326px) 100vw, 326px" /> </a>
                            <div class="mh-image-caption mh-posts-list-caption"> Politics</div>
                        </figure>
                        <div class="mh-posts-list-content clearfix">
                            <header class="mh-posts-list-header">
                                <h3 class="entry-title mh-posts-list-title"> <a href="single.php"> Tempor invidunt ut labore et dolore magna </a></h3>
                                <div class="mh-meta entry-meta">
                                  <span class="entry-meta-date updated">
                                    <i class="fa fa-clock-o"></i>
                                    <a href="single.php">Oct 2, 2015</a>
                                  </span>
                                  <span class="entry-meta-author author vcard">
                                    <i class="fa fa-user"></i>
                                    <a class="fn" href="single.php">John Doe</a>
                                  </span>
                                  <span class="entry-meta-comments">
                                    <i class="fa fa-comment-o"></i>
                                    <a href="single.php" class="mh-comment-count-link" >0</a>
                                  </span>
                                </div>
                            </header>
                            <div class="mh-posts-list-excerpt clearfix">
                                <div class="mh-excerpt">
                                    <p>Tatvero accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea consetetur sadipscing elitr,
                                      sed diam nonumy eirmod takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure
                                       <a class="mh-excerpt-more" href="single.php">[&#8230;]</a></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    <div class="mh-loop-pagination clearfix">
                        <nav class="navigation pagination" role="navigation">
                            <h2 class="screen-reader-text">Posts navigation</h2>
                            <div class="nav-links">
                              <span class='page-numbers current'>1</span>
                              <a class='page-numbers' href='page/2/index.html'>2</a>
                              <a class="next page-numbers" href="page/2/index.html">&raquo;</a>
                            </div>
                        </nav>
                    </div>
                </div>
                
                <aside class="mh-widget-col-1 mh-sidebar">
                  <div id="text-2" class="mh-widget widget_text">
                    <div class="textwidget">
                      <div class="mh-ad-spot">
                        <a href="#" target="_blank">
                          <img alt="MH Themes" src="images/ad-300x250-mh-magazine.png"> </a>
                        </div>
                      </div>
                    </div>

                      <?php include 'sidebar.php'; ?>

                  <div id="text-2" class="mh-widget widget_text">
                    <div class="textwidget">
                      <div class="mh-ad-spot">
                        <a href="#" target="_blank">
                          <img alt="MH Themes" src="images/ad-300x250-mh-magazine.png"> </a>
                        </div>
                      </div>
                    </div>
                  </aside>
                
            </div>
        </div>

        <?php include 'footer.php'; ?>
          