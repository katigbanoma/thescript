<?php include 'header.php'; ?>

    <div class="mh-header-nav-mobile clearfix"></div>
   
    <div class="mh-wrapper mh-home clearfix">
        <div class="mh-main mh-home-main">
            <div class="mh-home-columns clearfix">
                <div id="main-content" class="mh-content mh-home-content">

                    <!--- This egment is meant for my slider -->
                    <div id="mh_magazine_slider-2" class="mh-widget mh-home-2 mh-widget-col-2 mh_magazine_slider">
                        <div class="flexslider mh-slider-widget mh-slider-normal mh-slider-layout1">

                            <ul class="slides">
                                <li class="mh-slider-item">
                                    <article>
                                        <a href="#" title="">
                                  <img width="678" height="381" src="images/singa.jpg" class="attachment-mh-magazine-content
                                  size-mh-magazine-content wp-post-image" sizes="(max-width: 678px) 100vw, 678px" />
                                </a>
                                        <div class="mh-image-caption mh-slider-category"> World</div>
                                        <div class="mh-slider-caption">
                                            <div class="mh-slider-content">
                                                <h3 class="mh-slider-title">
                                                    <a href="#" title=""> Consequat vel illum dolore eu feugiat </a>
                                                </h3>
                                                <div class="mh-excerpt">
                                                    Tatvero accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea
                                                    <nav class="mh-social-icons mh-social-nav mh-social-nav-top clearfix">
                                                        <ul>Share
                                                            <li><a href="https://www.facebook.com/thescript"><i class="fa fa-mh-social"></i></a></li>
                                                            <li><a target="_blank" href="https://twitter.com/thescript"><i class="fa fa-mh-social"></i></a></li>
                                                            <li><a target="_blank" href="https://plus.google.com/thescript"><i class="fa fa-mh-social"></i></a></li>
                                                            <li><a target="_blank" href="https://www.youtube.com/thescript"><i class="fa fa-mh-social"></i></a></li>
                                                        </ul>
                                                    </nav>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                                <li class="mh-slider-item">
                                    <article>
                                        <a href="#" title="">
                                  <img width="678" height="381" src="images/singa.jpg" class="attachment-mh-magazine-content
                                  size-mh-magazine-content wp-post-image" sizes="(max-width: 678px) 100vw, 678px" />
                                </a>
                                        <div class="mh-image-caption mh-slider-category"> World</div>
                                        <div class="mh-slider-caption">
                                            <div class="mh-slider-content">
                                                <h3 class="mh-slider-title">
                                                    <a href="#" title=""> Consequat vel illum dolore eu feugiat </a>
                                                </h3>
                                                <div class="mh-excerpt">
                                                    Tatvero accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea
                                                    <nav class="mh-social-icons mh-social-nav mh-social-nav-top clearfix" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
                                                        <ul>Share
                                                            <li><a href="https://www.facebook.com/thescript"><i class="fa fa-mh-social"></i></a></li>
                                                            <li><a target="_blank" href="https://twitter.com/thescript"><i class="fa fa-mh-social"></i></a></li>
                                                            <li><a target="_blank" href="https://plus.google.com/thescript"><i class="fa fa-mh-social"></i></a></li>
                                                            <li><a target="_blank" href="https://www.youtube.com/thescript"><i class="fa fa-mh-social"></i></a></li>
                                                        </ul>
                                                    </nav>

                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                                <li class="mh-slider-item">
                                    <article>
                                        <a href="#" title="">
                                  <img width="678" height="381" src="images/singa.jpg" class="attachment-mh-magazine-content
                                  size-mh-magazine-content wp-post-image" sizes="(max-width: 678px) 100vw, 678px" />
                                </a>
                                        <div class="mh-image-caption mh-slider-category"> World</div>
                                        <div class="mh-slider-caption">
                                            <div class="mh-slider-content">
                                                <h3 class="mh-slider-title">
                                                    <a href="#" title=""> Consequat vel illum dolore eu feugiat </a>
                                                </h3>
                                                <div class="mh-excerpt">
                                                    Tatvero accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea
                                                    <nav class="mh-social-icons mh-social-nav mh-social-nav-top clearfix" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
                                                        <ul>Share
                                                            <li><a href="https://www.facebook.com/thescript"><i class="fa fa-mh-social"></i></a></li>
                                                            <li><a target="_blank" href="https://twitter.com/thescript"><i class="fa fa-mh-social"></i></a></li>
                                                            <li><a target="_blank" href="https://plus.google.com/thescript"><i class="fa fa-mh-social"></i></a></li>
                                                            <li><a target="_blank" href="https://www.youtube.com/thescript"><i class="fa fa-mh-social"></i></a></li>
                                                        </ul>
                                                    </nav>

                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--- This segment is meant for my slider -->

                    <div class="clearfix">
                        <div class="mh-widget-col-1 mh-sidebar mh-home-sidebar mh-home-area-3">
                            <div id="mh_magazine_custom_posts-3" class="mh-widget mh-home-3 mh_magazine_custom_posts">
                                <h4 class="mh-widget-title">
                                    <span class="mh-widget-title-inner">
                                <a href="single.php" class="mh-widget-title-link">Local</a>
                                <span style="padding:15px;">
                                  <div style="display:inline" style="font-size:5px; background:#000; border:1px solid #000; float:right">
                                    <a href="more.php" style="float:right; background-color:#ccc; font-size:8pt; padding:4px;">VIEW MORE</a>
                                  </div>
                                </span>
                                    </span>
                                </h4>
                                <ul class="mh-custom-posts-widget clearfix">
                                    <li class="mh-custom-posts-item mh-custom-posts-large clearfix post-170 post type-post status-publish format-standard has-post-thumbnail category-politics tag-debate tag-government tag-law tag-legislation tag-politics">
                                        <div class="mh-custom-posts-large-inner clearfix">
                                            <figure class="mh-custom-posts-thumb-xl">
                                                <a class="mh-thumb-icon mh-thumb-icon-small-mobile" href="single.php">
                                                    <div class="bg-content" style="background-image: url('images/local.png')"></div>
                                                </a>
                                            </figure>
                                            <div class="mh-custom-posts-content">
                                                <div class="mh-custom-posts-header">
                                                    <h3 class="mh-custom-posts-xl-title">
                                                        <a href="single.php" title="Laoreet dolore magna aliquam erat vol magna aliquyam">
                                          Maitama Sule: Osinbajo Visits Kano, Condoles With Residents
                                        </a></h3>
                                                    <div class="mh-meta entry-meta"> <span class="entry-meta-date updated"><i class="fa fa-clock-o"></i>
                                          <a href="#">Oct 11, 2015</a></span> <span class="entry-meta-comments"><i class="fa fa-comment-o"></i>
                                            <a href="#" class="mh-comment-count-link" >0</a></span></div>
                                                </div>
                                                <div class="mh-excerpt">
                                                    He said this during a one-day condolence visit to Kano State where he noted that he met the late politician for the first
                                                    time when he (Osinbajo)
                                                    <a class="mh-excerpt-more" href="#" title="Laoreet dolore magna aliquam erat vol magna aliquyam">[...]</a></div>
                                            </div>
                                        </div>
                                    </li>

                                    <h4 class="mh-widget-title">
                                        <span class="mh-widget-title-inner">
                                          <a href="single.php" class="mh-widget-title-link">Health</a>
                                          <span style="padding:15px;">
                                            <div style="display:inline" style="font-size:5px; background:#000; border:1px solid #000; float:right">
                                              <a href="more.php" style="float:right; background-color:#ccc; font-size:8pt; padding:4px;">VIEW MORE</a>
                                            </div>
                                          </span>
                                        </span>
                                    </h4>
                                    <ul class="mh-custom-posts-widget clearfix">
                                        <li class="mh-custom-posts-item mh-custom-posts-large clearfix post-170 post type-post status-publish format-standard has-post-thumbnail category-politics tag-debate tag-government tag-law tag-legislation tag-politics">
                                            <div class="mh-custom-posts-large-inner clearfix">
                                                <figure class="mh-custom-posts-thumb-xl">
                                                    <a class="mh-thumb-icon mh-thumb-icon-small-mobile" href="single.php" title="Laoreet dolore magna aliquam erat vol magna aliquyam">
                                                        <div class="bg-content" style="background-image: url('images/health.png')"></div>
                                                    </a>
                                                </figure>
                                                <div class="mh-custom-posts-content">
                                                    <div class="mh-custom-posts-header">
                                                        <h3 class="mh-custom-posts-xl-title"> <a href="single.php" title="Laoreet dolore magna aliquam erat vol magna aliquyam">
                                                  Ogun Govt. Shuts Over 180 Illegal Health Facilities </a></h3>
                                                        <div class="mh-meta entry-meta"> <span class="entry-meta-date updated"><i class="fa fa-clock-o"></i>
                                                    <a href="#">Oct 11, 2015</a></span>
                                                            <span class="entry-meta-comments">
                                                      <i class="fa fa-comment-o"></i>
                                                      <a href="#" class="mh-comment-count-link" >0</a>
                                                    </span>
                                                        </div>
                                                    </div>
                                                    <div class="mh-excerpt">
                                                        In a notice to commercial lenders, the financial regulator said the dollar auction would be both for spot and forward transactions,
                                                        to be settled
                                                        <a class="mh-excerpt-more" href="#" title="Laoreet dolore magna aliquam erat vol magna aliquyam">[...]</a></div>
                                                </div>
                                            </div>
                                        </li>
                                        <h4 class="mh-widget-title">
                                            <span class="mh-widget-title-inner">
                                                <a href="single.php" class="mh-widget-title-link">Economy</a>
                                                <span style="padding:15px;">
                                                  <div style="display:inline" style="font-size:5px; background:#000; border:1px solid #000; float:right">
                                                    <a href="more.php" style="float:right; background-color:#ccc; font-size:8pt; padding:4px;">VIEW MORE</a>
                                                  </div>
                                                </span>
                                            </span>
                                        </h4>
                                        <ul class="mh-custom-posts-widget clearfix">
                                            <li class="mh-custom-posts-item mh-custom-posts-large clearfix post-170 post type-post status-publish format-standard has-post-thumbnail category-politics tag-debate tag-government tag-law tag-legislation tag-politics">
                                                <div class="mh-custom-posts-large-inner clearfix">
                                                    <figure class="mh-custom-posts-thumb-xl">
                                                        <a class="mh-thumb-icon mh-thumb-icon-small-mobile" href="single.php" title="Laoreet dolore magna aliquam erat vol magna aliquyam">
                                                            <div class="bg-content" style="background-image: url('images/economy.png')"></div>
                                                        </a>
                                                    </figure>
                                                    <div class="mh-custom-posts-content">
                                                        <div class="mh-custom-posts-header">
                                                            <h3 class="mh-custom-posts-xl-title">
                                                                <a href="single.php" title="Laoreet dolore magna aliquam erat vol magna aliquyam">
                                                            CBN Plans $100m Sale At Special Auction</a></h3>
                                                            <div class="mh-meta entry-meta"> <span class="entry-meta-date updated">
                                                              <i class="fa fa-clock-o"></i><a href="#">Oct 11, 2015</a></span>
                                                                <span class="entry-meta-comments"><i class="fa fa-comment-o"></i>
                                                                <a href="#" class="mh-comment-count-link" >0</a>
                                                              </span>
                                                            </div>
                                                        </div>
                                                        <div class="mh-excerpt">
                                                            In a notice to commercial lenders, the financial regulator said the dollar auction would be both for spot and forward transactions,
                                                            to be settled
                                                            <a class="mh-excerpt-more" href="#" title="Laoreet dolore magna aliquam erat vol magna aliquyam">[...]</a></div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                            </div>
                        </div>
                        <div class="mh-widget-col-1 mh-sidebar mh-home-sidebar mh-margin-left mh-home-area-4">
                            <div id="mh_magazine_custom_posts-4" class="mh-widget mh-home-4 mh_magazine_custom_posts">
                                <h4 class="mh-widget-title">
                                    <span class="mh-widget-title-inner">
                                                        <a href="single.php" class="mh-widget-title-link">Politics</a>
                                                      </span>
                                    <span style="padding:15px;">
                                                        <div style="display:inline" style="font-size:5px; background:#000; border:1px solid #000; float:right">
                                                          <a href="more.php" style="float:right; background-color:#ccc; font-size:8pt; padding:4px;">VIEW MORE</a>
                                                        </div>
                                                      </span>
                                </h4>
                                <ul class="mh-custom-posts-widget clearfix">
                                    <li class="mh-custom-posts-item mh-custom-posts-large clearfix post-52 post type-post status-publish format-video has-post-thumbnail category-economy tag-city tag-economy tag-europe tag-financial tag-london tag-uk post_format-post-format-video">
                                        <div class="mh-custom-posts-large-inner clearfix">
                                            <figure class="mh-custom-posts-thumb-xl">
                                                <a class="mh-thumb-icon mh-thumb-icon-small-mobile" href="single.php" title="Esse molestie consequat vel illum dolore eu feugiat">
                                                    <div class="bg-content" style="background-image: url('images/politics.png')"></div>
                                                </a>
                                            </figure>
                                            <div class="mh-custom-posts-content">
                                                <div class="mh-custom-posts-header">
                                                    <h3 class="mh-custom-posts-xl-title"> <a href="single.php" title="Esse molestie consequat vel illum dolore eu feugiat">
                                                                  Saraki’s CCT acquittal: Fed Govt Completes record transmission. </a>
                                                    </h3>
                                                    <div class="mh-meta entry-meta"> <span class="entry-meta-date updated"><i class="fa fa-clock-o"></i>
                                                                  <a href="#">Oct 12, 2015</a></span> <span class="entry-meta-comments"><i class="fa fa-comment-o"></i>
                                                                    <a href="#" class="mh-comment-count-link" >0</a></span></div>
                                                </div>
                                                <div class="mh-excerpt">
                                                    The office of the Attorney General of the Federation (AGF) yesterday completed the transmission of record of proceedings
                                                    <a class="mh-excerpt-more" href="#" title="Esse molestie consequat vel illum dolore eu feugiat">[...]</a></div>
                                            </div>
                                        </div>
                                    </li>
                                    <h4 class="mh-widget-title">
                                        <span class="mh-widget-title-inner">
                                                                  <a href="single.php" class="mh-widget-title-link">Entertainment</a>
                                                                </span>
                                        <span style="padding:15px;">
                                                                  <div style="display:inline" style="font-size:5px; background:#000; border:1px solid #000; float:right">
                                                                    <a href="more.php" style="float:right; background-color:#ccc; font-size:8pt; padding:4px;">VIEW MORE</a>
                                                                  </div>
                                                                </span>
                                    </h4>
                                    <ul class="mh-custom-posts-widget clearfix">
                                        <li class="mh-custom-posts-item mh-custom-posts-large clearfix post-52 post type-post status-publish format-video has-post-thumbnail category-economy tag-city tag-economy tag-europe tag-financial tag-london tag-uk post_format-post-format-video">
                                            <div class="mh-custom-posts-large-inner clearfix">
                                                <figure class="mh-custom-posts-thumb-xl">
                                                    <a class="mh-thumb-icon mh-thumb-icon-small-mobile" href="single.php" title="Esse molestie consequat vel illum dolore eu feugiat">
                                                        <div class="bg-content" style="background-image: url('images/entertainments.png')"></div>
                                                    </a>
                                                </figure>
                                                <div class="mh-custom-posts-content">
                                                    <div class="mh-custom-posts-header">
                                                        <h3 class="mh-custom-posts-xl-title">
                                                            <a href="single.php" title="Esse molestie consequat vel illum dolore eu feugiat">
                                                                              What is my crime? Harrysong asks after getting sued </a></h3>
                                                        <div class="mh-meta entry-meta"> <span class="entry-meta-date updated"><i class="fa fa-clock-o"></i><a href="#">Oct 12, 2015</a></span>
                                                            <span class="entry-meta-comments"><i class="fa fa-comment-o"></i><a href="#" class="mh-comment-count-link" >0</a></span>
                                                        </div>
                                                    </div>
                                                    <div class="mh-excerpt">
                                                        The pair has been at odds since Harrysong left Five Star Music, a record label owned by Kcee and his brother Emeka Okonkwo.
                                                        Soso Soberekon,
                                                        <a class="mh-excerpt-more" href="#" title="Esse molestie consequat vel illum dolore eu feugiat">[...]</a></div>
                                                </div>
                                            </div>
                                        </li>
                                        <h4 class="mh-widget-title">
                                            <span class="mh-widget-title-inner">
                                                                            <a href="single.php" class="mh-widget-title-link">Sports</a>
                                                                          </span>
                                            <span style="padding:15px;">
                                                                            <div style="display:inline" style="font-size:5px; background:#000; border:1px solid #000; float:right">
                                                                              <a href="more.php" style="float:right; background-color:#ccc; font-size:8pt; padding:4px;">VIEW MORE</a>
                                                                            </div>
                                                                          </span>
                                        </h4>
                                        <ul class="mh-custom-posts-widget clearfix">
                                            <li class="mh-custom-posts-item mh-custom-posts-large clearfix post-52 post type-post status-publish format-video has-post-thumbnail category-economy tag-city tag-economy tag-europe tag-financial tag-london tag-uk post_format-post-format-video">
                                                <div class="mh-custom-posts-large-inner clearfix">
                                                    <figure class="mh-custom-posts-thumb-xl">
                                                        <a class="mh-thumb-icon mh-thumb-icon-small-mobile" href="single.php" title="Esse molestie consequat vel illum dolore eu feugiat">
                                                            <div class="bg-content" style="background-image: url('images/sport.png')"></div>
                                                        </a>
                                                    </figure>
                                                    <div class="mh-custom-posts-content">
                                                        <div class="mh-custom-posts-header">
                                                            <h3 class="mh-custom-posts-xl-title"> <a href="single.php" title="Esse molestie consequat vel illum dolore eu feugiat">
                                                                                    Man United agree £75m deal with Everton for Romelu Lukaku
                                                                                  </a>
                                                            </h3>
                                                            <div class="mh-meta entry-meta"> <span class="entry-meta-date updated"><i class="fa fa-clock-o"></i>
                                                                                  <a href="#">Oct 12, 2015</a></span>                                                                    <span class="entry-meta-comments"><i class="fa fa-comment-o"></i><a href="#" class="mh-comment-count-link" >0</a></span></div>
                                                        </div>
                                                        <div class="mh-excerpt">
                                                            The deal is expected to be finalized this weekend, Lukaku was Mourinho’s main summer transfer target, and now that the deal
                                                            is in the bag,
                                                            <a class="mh-excerpt-more" href="#" title="Esse molestie consequat vel illum dolore eu feugiat">[...]</a></div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <style media="screen">
                                            .bg-content {
                                                width: 326px;
                                                height: 245px;
                                                background-size: cover;
                                            }
                                        </style>
                            </div>
                        </div>
                    </div>
                    <div class="textwidget">
                        <div class="mh-ad-spot">
                            <a target="_blank" href="#">
                                <img width="680" height="130" style="height:130px;" src="images/banner1.png" />
                              </a>
                        </div>
                    </div>
                </div>
                
                <div class="mh-widget-col-1 mh-sidebar mh-home-sidebar mh-home-area-6">
                    <div id="mh_magazine_youtube-2" class="mh-widget mh_magazine_youtube">
                        <h4 class="mh-widget-title"><span class="mh-widget-title-inner"><i class="fa fa-youtube-play"></i>Featured Video</span></h4>
                        <div class="mh-video-widget">
                            <div class="mh-video-container">
                                <iframe seamless width="1280" height="720" src="http://www.youtube.com/embed/WBgwuFM92i4?wmode=opaque&amp;hd=1&amp;autoplay=0&amp;showinfo=0&amp;controls=0&amp;rel=0"
                                    allowfullscreen></iframe>
                            </div>
                            <p style="font-size: 12px; font-weight:bold; padding-top: 5px; padding-bottom:5px;">
                                <a href="#">
                                <i class="fa fa-chevron-right" style="padding-top: 5px; padding-bottom:5px"></i>About Obama at the G8 world leaders summit:
                              </a>
                            </p>
                        </div>
                        <p style="padding: 6px 6px;">Up Next</p>
                        <ul class="mh-custom-posts-widget clearfix">
                            <li class="mh-custom-posts-item mh-custom-posts-small clearfix post-144 post type-post status-publish format-standard has-post-thumbnail category-world tag-boat tag-sightseeing tag-travel tag-world">
                                <figure class="mh-custom-posts-thumb">
                                    <a class="mh-thumb-icon mh-thumb-icon-small" href="#" title="Iam nonumy eirmod tempor invidunt ut labore et dol">
                                    <img width="80" height="60" src="images/Picture1.png" class="attachment-mh-magazine-small size-mh-magazine-small wp-post-image" alt="Ship"  sizes="(max-width: 80px) 100vw, 80px" /> </a>
                                </figure>
                                <div class="mh-custom-posts-header">
                                    <div class="mh-custom-posts-small-title"> <a href="#" style="font-size:12px;"> Maitama Sule: Osinbajo Visits Kano, Condoles With Residents</a></div>
                                    <div class="mh-meta entry-meta">
                                        <span class="entry-meta-date updated" style="color:black;">
                                        <i class="fa fa-play fa-1g" aria-hidden="true"></i>
                                        <a href="#" style="font-weight:bold; color:black;">Play Video 6:33</a>
                                      </span>
                                    </div>
                                </div>
                            </li>
                            <li class="mh-custom-posts-item mh-custom-posts-small clearfix post-144 post type-post status-publish format-standard has-post-thumbnail category-world tag-boat tag-sightseeing tag-travel tag-world">
                                <figure class="mh-custom-posts-thumb">
                                    <a class="mh-thumb-icon mh-thumb-icon-small" href="#" title="Iam nonumy eirmod tempor invidunt ut labore et dol">
                                      <img width="80" height="60" src="images/Picture4.png" class="attachment-mh-magazine-small size-mh-magazine-small wp-post-image" alt="Ship"  sizes="(max-width: 80px) 100vw, 80px" /> </a>
                                </figure>
                                <div class="mh-custom-posts-header">
                                    <div class="mh-custom-posts-small-title"> <a href="#" style="font-size:12px;"> Saraki’s CCT acquittal: Fed Govt Completes record transmission.</a></div>
                                    <div class="mh-meta entry-meta">
                                        <span class="entry-meta-date updated" style="color:black;">
                                          <i class="fa fa-play fa-1g" aria-hidden="true"></i>
                                          <a href="#" style="font-weight:bold; color:black;">Play Video 6:33</a>
                                        </span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div id="mh_magazine_custom_posts-2" class="mh-widget mh-home-6 mh_magazine_custom_posts">
                        <h4 class="mh-widget-title"><span class="mh-widget-title-inner">IN THE NEWS</span></h4>
                        <ul class="mh-custom-posts-widget clearfix">
                            <li class="mh-custom-posts-item mh-custom-posts-small clearfix post-140 post type-post status-publish format-standard has-post-thumbnail category-world tag-festival tag-sightseeing tag-travel tag-world">
                                <figure class="mh-custom-posts-thumb">
                                    <a class="mh-thumb-icon mh-thumb-icon-small" href="#" title="Hendrerit in vulputate velit esse molestie consequat">
                                        <img width="80" height="60" src="images/Picture3.png" class="attachment-mh-magazine-small size-mh-magazine-small wp-post-image" alt="Light Garland" sizes="(max-width: 80px) 100vw, 80px" /> </a>
                                </figure>
                                <div class="mh-custom-posts-header">
                                    <div class="mh-custom-posts-small-title"> <a href="#" title="Hendrerit in vulputate velit esse molestie consequat">Saraki’s CCT acquittal: Fed. Govt. Completes record transmission. </a></div>
                                    <div class="mh-meta entry-meta"> <span class="entry-meta-date updated"><i class="fa fa-clock-o"></i><a href="#">Oct 4, 2015</a></span>
                                        <span class="entry-meta-comments"><i class="fa fa-comment-o"></i><a href="#" class="mh-comment-count-link" >6</a></span></div>
                                </div>
                            </li>
                            <li class="mh-custom-posts-item mh-custom-posts-small clearfix post-140 post type-post status-publish format-standard has-post-thumbnail category-world tag-festival tag-sightseeing tag-travel tag-world">
                                <figure class="mh-custom-posts-thumb">
                                    <a class="mh-thumb-icon mh-thumb-icon-small" href="#" title="Hendrerit in vulputate velit esse molestie consequat">
                                            <img width="80" height="60" src="images/Picture1.png" class="attachment-mh-magazine-small size-mh-magazine-small wp-post-image" alt="Light Garland" sizes="(max-width: 80px) 100vw, 80px" /> </a>
                                </figure>
                                <div class="mh-custom-posts-header">
                                    <div class="mh-custom-posts-small-title"> <a href="#" title="Hendrerit in vulputate velit esse molestie consequat">
                                              Hendrerit in vulputate velit esse molestie consequat </a></div>
                                    <div class="mh-meta entry-meta"> <span class="entry-meta-date updated"><i class="fa fa-clock-o"></i><a href="#">Oct 4, 2015</a></span>
                                        <span class="entry-meta-comments"><i class="fa fa-comment-o"></i><a href="#" class="mh-comment-count-link" >6</a></span></div>
                                </div>
                            </li>
                            <li class="mh-custom-posts-item mh-custom-posts-small clearfix post-108 post type-post status-publish format-standard has-post-thumbnail category-money tag-banks tag-credit-cards tag-financial tag-money">
                                <figure class="mh-custom-posts-thumb">
                                    <a class="mh-thumb-icon mh-thumb-icon-small" href="#" title="Sea takimata sanctus est lorem ipsum dolor sit">
                                                  <img width="80" height="60" src="images/Picture2.png" class="attachment-mh-magazine-small size-mh-magazine-small wp-post-image" alt="Credit Card" sizes="(max-width: 80px) 100vw, 80px" /> </a>
                                </figure>
                                <div class="mh-custom-posts-header">
                                    <div class="mh-custom-posts-small-title"> <a href="#" title="Sea takimata sanctus est lorem ipsum dolor sit">Maitama Sule: Osinbajo Visits Kano, Condoles With Residents</a></div>
                                    <div class="mh-meta entry-meta"> <span class="entry-meta-date updated"><i class="fa fa-clock-o"></i><a href="#">Jul 10, 2015</a></span>                                            <span class="entry-meta-comments"><i class="fa fa-comment-o"></i><a href="#" class="mh-comment-count-link" >3</a></span></div>
                                </div>
                            </li>
                            <li class="mh-custom-posts-item mh-custom-posts-small clearfix post-75 post type-post status-publish format-standard has-post-thumbnail category-europe tag-europe tag-mobility tag-trainstation tag-travel">
                                <figure class="mh-custom-posts-thumb">
                                    <a class="mh-thumb-icon mh-thumb-icon-small" href="#" title="Vulputate velit esse molestie consequat vel illum">
                                                    <img width="80" height="60" src="images/Picture1.png" class="attachment-mh-magazine-small size-mh-magazine-small wp-post-image" alt="Architecture"  sizes="(max-width: 80px) 100vw, 80px" /> </a>
                                </figure>
                                <div class="mh-custom-posts-header">
                                    <div class="mh-custom-posts-small-title"> <a href="#" title="Vulputate velit esse molestie consequat vel illum">Saraki’s CCT acquittal: Fed. Govt. Completes record transmission. </a></div>
                                    <div class="mh-meta entry-meta"> <span class="entry-meta-date updated"><i class="fa fa-clock-o"></i><a href="#">Mar 10, 2015</a></span>
                                        <span class="entry-meta-comments"><i class="fa fa-comment-o"></i>
                                                        <a href="" class="mh-comment-count-link" >2</a></span></div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <?php include 'sidebar.php'; ?>

                  </div>
            </div>
        </div>
    </div>

<?php include 'footer.php'; ?>

