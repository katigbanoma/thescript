<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <script src="js/jq0.js"></script>
    <script src="js/jq1.js"></script>
    <script src="js/jq2.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>The Script</title>
    <link rel="canonical" href="https://www.mhthemes.com/themes/mh/magazine/" />
    <style type="text/css" id="custom-background-css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important
        }

        .flex-direction-nav {
            display: none
        }

        body.custom-background {
            background-image: url("images/background-mhmag1.jpg");
            background-position: right top;
            background-size: cover;
            background-repeat: no-repeat;
            background-attachment: fixed
        }

        ul.social-links {
            margin: 10px 0;
        }

        ul.social-links li {
            padding: 0 10px;
            color: white;
        }
    </style>
</head>

<body id="mh-mobile" class="home page-template page-template-template-homepage page-template-template-homepage-php page page-id-6 custom-background wp-custom-logo mh-boxed-layout mh-right-sb mh-loop-layout1 mh-widget-layout1 mh-loop-hide-caption">
    <div class="mh-container mh-container-outer">
        <div class="mh-header-nav-mobile clearfix"></div>
        <div class="mh-preheader">
            <div class="mh-container mh-container-inner mh-row clearfix">
                <div class="mh-header-bar-content mh-header-bar-top-left mh-col-2-3 clearfix">
                    <nav class="mh-navigation mh-header-nav mh-header-nav-top clearfix">
                        <div class="menu-header-container">
                            <ul id="menu-header" class="menu social-links">
                                <li>Monday July 3, 2017</li>
                                <li><a href="https://facebook.com/thescript"><i class="fa fa-facebook fa-2x"></i></a></li>
                                <li><a href="https://twitter.com/thescript"><i class="fa fa-twitter fa-2x"></i></a></li>
                                <li><a href="https://google.com/thescript"><i class="fa fa-google-plus fa-2x"></i></a></li>
                                <li><a href="https://youtube.com/thescript"><i class="fa fa-youtube fa-2x"></i></a></li>
                            </ul>
                        </div>
                    </nav>
                </div>

                <div class="mh-header-bar-content mh-header-bar-bottom-right mh-col-1-3 clearfix">
                    <aside class="mh-header-search mh-header-search-bottom">
                        <form role="search" method="get" class="search-form" action="">
                            <input type="search" placeholder="Search" />
                            <a href="#" id="search-btn">
              <i class="fa fa-search"></i>
            </a>
                            <button type="button" name="button" class="btn-sub">
              Subcribe!
            </button>
                        </form>
                    </aside>
                </div>
                <style>
                    .btn-sub {
                        margin: 5px 0px 5px 5px;
                        background-color: red;
                        color: white;
                        border: none;
                        -moz-box-shadow: inset 0 0 10px #000000;
                        -webkit-box-shadow: inset 0 0 10px #000000;
                        box-shadow: inset 0 0 10px #000000;
                        font-family: calibri;
                        padding: 3px 15px;
                        border-radius: 5px;
                    }

                    #search-btn {
                        background-color: #666;
                        margin-left: -3px;
                        color: white;
                        padding: 3px 3px 4px 3px
                    }
                </style>
            </div>
        </div>
        <header class="mh-header" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
            <div class="mh-container mh-container-inner" style=" border-bottom:2px groove red;">
                <div class="mh-custom-header clearfix">
                    <div class="mh-header-columns mh-row clearfix">
                        <div class="mh-col-1-3 mh-site-identity" style="margin-left:360px;">
                            <div class="mh-site-logo" role="banner" itemscope="itemscope" itemtype="http://schema.org/Brand">
                                <a href="index.php" class="custom-logo-link" rel="home" itemprop="url">
                <img width="300" height="100" src="images/thescript.PNG" class="custom-logo" alt="" itemprop="logo" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="textwidget">
                <div class="mh-ad-spot">
                    <a target="_blank" href="#">
            <img width="1128" height="90" style="height:120px;" src="images/banner2.png" /> </a>
                </div>
            </div>

            <div class="mh-main-nav-wrap">
                <nav class="mh-navigation mh-main-nav mh-container mh-container-inner clearfix" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
                    <div class="menu-navigation-container">
                        <ul id="menu-navigation" class="menu">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="single.php">Politics</a></li>
                            <li><a href="single.php">World</a></li>
                            <li><a href="single.php">Nigeria</a></li>
                            <li><a href="single.php">Economy</a></li>
                            <li><a href="single.php">Health</a></li>
                            <li><a href="#">More</a>
                                <ul class="sub-menu">
                                    <li><a href="#"> Education</a></li>
                                    <li><a href="#">  Music</a></li>
                                    <li><a href="#">Comedy</a></li>
                                    <li><a href="#"> Money</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>

        <div class="mh-subheader">
            <div class="mh-container mh-container-inner mh-row clearfix">
                <div class="mh-header-bar-content mh-header-bar-bottom-left mh-col-2-3 clearfix">
                    <div class="mh-header-ticker mh-header-ticker-bottom">
                        <div class="mh-ticker-bottom">
                            <div class="mh-ticker-title mh-ticker-title-bottom"> BREAKING NEWS<i class="fa fa-chevron-right"></i></div>
                            <div class="mh-ticker-content mh-ticker-content-bottom">
                                <ul id="mh-ticker-loop-bottom">
                                    <li class="mh-ticker-item mh-ticker-item-bottom">
                                        <a href="single.php" title="Consequat vel illum dolore eu feugiat">
                        <span class="mh-ticker-item-date mh-ticker-item-date-bottom"> [ Oct 17, 2015 ] </span>
                        <span class="mh-ticker-item-title mh-ticker-item-title-bottom"> Consequat vel illum dolore eu feugiat </span>
                        <span class="mh-ticker-item-cat mh-ticker-item-cat-bottom"> <i class="fa fa-caret-right"></i> World </span>
                      </a>
                                    </li>
                                    <li class="mh-ticker-item mh-ticker-item-bottom">
                                        <a href="single.php" title="Kalil delenit augue duis dolore hetura">
                        <span class="mh-ticker-item-date mh-ticker-item-date-bottom"> [ Oct 16, 2015 ] </span> <span class="mh-ticker-item-title mh-ticker-item-title-bottom">
                          Kalil delenit augue duis dolore hetura </span> <span class="mh-ticker-item-cat mh-ticker-item-cat-bottom"> <i class="fa fa-caret-right"></i> Politics </span> </a>
                                    </li>
                                    <li class="mh-ticker-item mh-ticker-item-bottom">
                                        <a href="single.php" title="Zaril delenit augue duis dolore te feugait nulla">
                            <span class="mh-ticker-item-date mh-ticker-item-date-bottom"> [ Oct 15, 2015 ] </span> <span class="mh-ticker-item-title mh-ticker-item-title-bottom">
                              Zaril delenit augue duis dolore te feugait nulla </span> <span class="mh-ticker-item-cat mh-ticker-item-cat-bottom"> <i class="fa fa-caret-right"></i> World </span> </a>
                                    </li>
                                    <li class="mh-ticker-item mh-ticker-item-bottom">
                                        <a href="single.php" title="Kasd gubergren no sea takimata sanctus est"> <span class="mh-ticker-item-date mh-ticker-item-date-bottom">
                                [ Oct 13, 2015 ] </span> <span class="mh-ticker-item-title mh-ticker-item-title-bottom"> Kasd gubergren no sea takimata sanctus est
                                </span> <span class="mh-ticker-item-cat mh-ticker-item-cat-bottom"> <i class="fa fa-caret-right"></i> Business </span> </a>
                                    </li>
                                    <li class="mh-ticker-item mh-ticker-item-bottom">
                                        <a href="single.php" title="Esse molestie consequat vel illum dolore eu feugiat">
                                  <span class="mh-ticker-item-date mh-ticker-item-date-bottom"> [ Oct 12, 2015 ] </span> <span class="mh-ticker-item-title mh-ticker-item-title-bottom">
                                    Esse molestie consequat vel illum dolore eu feugiat </span>
                                    <span class="mh-ticker-item-cat mh-ticker-item-cat-bottom"> <i class="fa fa-caret-right"></i>
                                      Economy
                                    </span>
                                  </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>